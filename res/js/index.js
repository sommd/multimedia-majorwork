function removeClassName(elem, className) {
    elem.className = elem.className.replace(new RegExp("\\b" + className + "\\b"), "");
}

function addClassName(elem, className) {
    elem.className = elem.className + " " + className;
}

var timeoutId = null;
var timeoutFun = null;

function updatePagerItem(animate) {
    var item = window.location.hash.substr(1) || MAJOR_WORK;

    function select(parent) {
        // Deselect old item
        removeClassName(parent.querySelector(".selected"), "selected");
        // Select new item
        addClassName(parent.querySelector("#" + item), "selected");
    }

    select(document.querySelector("#nav #list"));

    function selectAnimated(parent) {
        // Skip the animation to the end in literally the worst way possible but I'm bad at js so whatever.
        if (timeoutId != null) {
            clearTimeout(timeoutId);
            timeoutFun();
            if (timeoutId != null) {
                clearTimeout(timeoutId);
                timeoutFun();
            }
        }

        var oldItem = parent.querySelector(".selected");
        var newItem = parent.querySelector("#" + item);

        // Fadeout old item
        addClassName(oldItem, "fadeout");

        timeoutId = setTimeout(timeoutFun = function () {
            // Reset fadeout
            removeClassName(oldItem, "fadeout");

            // Deselect old item
            removeClassName(oldItem, "selected");
            // Select new item
            addClassName(newItem, "selected");

            // Fadein new item
            addClassName(newItem, "fadein");
            timeoutId = setTimeout(timeoutFun = function () {
                // Reset fadein
                removeClassName(newItem, "fadein");

                timeoutId = null;
                timeoutFun = null;
            }, 500);
        }, 500);
    }

    var pager = document.querySelector("#pager");
    if (animate) {
        selectAnimated(pager);
    } else {
        select(pager);
    }
}

document.addEventListener("DOMContentLoaded", function () { updatePagerItem(false); });
window.addEventListener("hashchange", function () { updatePagerItem(true); });
